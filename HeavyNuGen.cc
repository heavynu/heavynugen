#include <iostream>
#include "TLorentzVector.h"
#include "TRandom3.h"
#include <cmath>
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TInterpreter.h"
#include "TROOT.h"

//using namespace TMath;
using namespace std;

double M_W = 1000;
double M_nu = 500;
double Gamma_W=2;//The same as in SM
double Gamma_nu=1;//random

double Sigma(TLorentzVector p_1 , TLorentzVector p_2 ,TLorentzVector  k_1,
TLorentzVector  k_2 ,  TLorentzVector  q_1 , TLorentzVector q_2 , char type)
{
// cout << "p_1\t"<< p_1.E()<<"\n";
  //cout << "p_2\t"<< p_2.E()<<"\n";
  //cout << "k_1\t"<< k_1.E()<<"\n";
  //cout << "k_2\t"<< k_2.E()<<"\n";
  //cout << "q_1\t"<< q_1.E()<<"\n";
  //cout << "q_2\t"<< q_2.E()<<"\n";

  double alpha = pow(137,-1);
  double SinTheta_W = sqrt(0.23);
  double e = sqrt(4*TMath::Pi()*alpha);
   //cout << "e\t"<< e <<"\n";
   //cout << "pi\t"<< TMath::Pi()<<"\n";
   //cout << "alpha\t"<< alpha <<"\n";


  double g = e/SinTheta_W;    
   //cout << "g\t"<< g <<"\n";

  //TVector3 L = p_1.Vect()+p_2.Vect()-k_1.Vect()-k_2.Vect();
  TVector3 q_1_Spatial = q_1.Vect();
   TVector3 q_2_Spatial = q_2.Vect();
  double theta =  q_1_Spatial.Angle(q_1_Spatial + q_2_Spatial);
   //cout << "theta\t"<< theta <<"\n";
 
 double q_1_mod = sqrt(q_1_Spatial*q_1_Spatial);
   double q_2_mod = sqrt(q_2_Spatial*q_2_Spatial);
  double q_12_mod = 
sqrt((q_1_Spatial+q_2_Spatial)*(q_1_Spatial+q_2_Spatial));

  double Traces = 0;
//Dirac case
if (type == 'D') Traces = (k_1*p_2)*(q_2*k_2)*   ( (k_1*p_2)*(p_1*q_1) + (p_1*(k_1-p_2))*(q_1*(k_1-p_2))   );

//Majorana case
if (type == 'M') Traces = 2*M_nu*M_nu* (k_1*p_2)*(p_1*q_1)*(q_2*k_2);

  double DSigma = 4*pow(g,8)  *pow(2*TMath::Pi() ,-8) * pow(    
(p_1*p_2)* 
( pow( (  (p_1+p_2)*(p_1+p_2) - 
pow(M_W,2)  ),2) + pow(M_W*Gamma_W,2) )       * 
(pow( ( (q_1+q_2)*(q_1+q_2) - pow(M_W,2) ),2) +  pow(M_W*Gamma_W,2)) *   
(pow(((p_1+p_2-k_1)*(p_1+p_2-k_1) - pow(M_nu,2) ),2)+  
pow(M_nu*Gamma_nu,2) ) ,-1 ) * Traces *  
sin(theta)*q_1.E()*pow(k_1.E()*k_2.E()*(q_1_mod + q_2_mod + 
q_12_mod*cos(theta) ),-1);

//cout<<theta<<"\n";
//cout << "Sigma\t" << DSigma << "\n";
return DSigma;
}


int main ()
{
  int Par = 1;
  double E = 500;  //The energy of initial quark
  TLorentzVector p1(0,0,E,E); 
  TLorentzVector p2(0,0,-E,E);
  
  //cout << p1.Px() <<"\t"<< p1.Py()<<"\t"<<p1.Pz()<<"\t"<<p1.E()<<"\n";
   //cout << p2.Px() <<"\t"<< p2.Py()<<"\t"<<p2.Pz()<<"\t"<<p2.E()<<"\n";

  TLorentzVector k1;
  TLorentzVector k2;
  TLorentzVector q1;
  TLorentzVector q2;

  TLorentzVector k1_max(E,E,E,E);
  TLorentzVector k2_max; 
   TLorentzVector k2_min;
 //(E,E,E,E);
  TLorentzVector q1_max;
    //(E,E,E,E);
 // TLorentzVector q2_max;//(E,E,E,E);

  double sigma_max = 0 ;
 double sigma_max_D = 0 ;
 double sigma_max_M = 0 ;

   TRandom r;



  int N=2000000000; //Number of steps
//  int N = 1000000;
  for(int i = 0 ; i < N ; i++)
  {
    //cout<<"The first cycle. i \t"<<i<<"\n";
    k1[0] = r.Uniform(-k1_max.Px() , k1_max.Px());
//    k1_max[1] = sqrt(pow(k1_max.E(),2) - pow(k1.Px(),2)  );
    k1[1] = r.Uniform(-k1_max.Py() , k1_max.Py());
//    k1_max[2] = sqrt(pow(k1_max.E(),2) - pow(k1.Px(),2) - pow(k1.Py(),2));
    k1[2] = r.Uniform(-k1_max.Pz() , k1_max.Pz());
    k1[3] = sqrt(pow(k1.Px() , 2) + pow(k1.Py() , 2 ) + pow(k1.Pz() , 2));
//     cout<<"k1\t" << k1.Px() <<"\t"<< k1.Py()<<"\t"<<k1.Pz()<<"\t"<<k1.E()<<"\n";

    if (k1.E() > p1.E() + p2.E() ) continue;    
  
//    k2_max[3] = p1.E()+p2.E() - k1.E();
    if (k1.Px() >= 0 )   k2_max[0] = E -  k1.Px();
      else k2_max[0] = E;
    if (k1.Px() <= 0)  k2_min[0] = -E -  k1.Px();
      else k2_min[0] = -E;


    k2(0) = r.Uniform(k2_min.Px() , k2_max.Px());
    k2_max[1] = TMath::Min( sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2)) - 
k1.Py()   , 
sqrt(pow(E,2) - pow(k2.Px(),2)  )   );
      k2_min[1] = TMath::Max(  - sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2)) - 
k1.Py()  ,   -sqrt(pow(E,2) - pow(k2.Px(),2)  )       );
    k2(1) =  r.Uniform(k2_min.Py() , k2_max.Py());
    k2_max[2] = TMath::Min(sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2) - 
pow(k1.Py()+k2.Py(),2)) - k1.Py()   ,   sqrt(pow(E,2) - 
pow(k2.Px(),2) -pow(k2.Py(),2))   );
    k2_min[1] = TMath::Max(- sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2) - 
pow(k1.Py()+k2.Py(),2)) - k1.Py()   ,  - sqrt(pow(E,2) - 
pow(k2.Px(),2) - pow(k2.Py(),2))  );
    k2(2) =  r.Uniform(k2_min.Pz() , k2_max.Pz());
    k2(3) = sqrt(pow(k2.Px() , 2) + pow(k2.Py() , 2 ) + pow(k2.Pz() , 2));
    if (k2.E() > p1.E() + p2.E() - k1.E() ) continue;

  //     cout<<"k2\t" << k2.Px() <<"\t"<<k2.Py()<<"\t"<<k2.Pz()<<"\t"<<k2.E()<<"\n";
  // cout<<"k2_max\t" << k2_max.Px() <<"\t"<<k2_max.Py()<<"\t"<<k2_max.Pz()<<"\t"<<k2_max.E()<<"\n";
//cout << "k2^2 \t"<<k2*k2 <<"\n";


   q1_max[3] =  TMath::Min(  p1.E()+p2.E() - k1.E()-k2.E()  ,  E);
//    q1_max[0] = q1_max.E();
//    q1_max[1] = q1_max.E();
//    q1_max[2] = q1_max.E();

      TVector3 k1_spa = k1.Vect();
      TVector3 k2_spa = k2.Vect();
      TVector3 L = k1_spa + k2_spa;
     // L(3) = sqrt(pow(L.Px() , 2) + pow(L.Py() , 2 ) + pow(L.Pz() , 2));
  
    double Theta =  r.Uniform(0 , TMath::Pi());
     double Phi = r.Uniform(0 , 2*TMath::Pi());
    

    q1(3) = (pow(2*E-sqrt(k1_spa*k1_spa)-sqrt(k2_spa*k2_spa) ,2) - 
(k1_spa+k2_spa)*(k1_spa+k2_spa)  ) *pow(2*( 2*E - sqrt(k1_spa*k1_spa) - 
sqrt(k2_spa*k2_spa) + sqrt((k1_spa+k2_spa)*(k1_spa+k2_spa))*cos(Theta)  
),-1);
    if (q1.E() < 0) 
      {
     //cout << "q1.E < 0 \n";
      continue;
      }
    if ( p1.E() + p2.E() - k1.E() - k2.E() - q1.E() < 0) continue;
    TVector3 e1;
    TVector3 e2;
    TVector3 e3;
    e1(0) = 0;
    e1(1) = L.Z()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
    e1(2) = -L.Y()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
//cout <<"e1\t"  << e1.X() <<"\t"<< e1.Y() <<"\t"<< e1.Z()<<"\n";

    e2(0) = (pow(L.Y(),2) + pow(L.Z(),2))*pow( pow(pow(L.Y(),2) + 
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5); 
    e2(1) = -L.X()*L.Y()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);
    e2(2) = -L.X()*L.Z()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);

//cout <<"e2\t" << e2.X() <<"\t"<< e2.Y() <<"\t"<< e2.Z()<<"\n";
  
//cout<<"phi\t"<<Phi<<"\n";
    e3 = pow( (L*L) ,-0.5)* L;
//e3 = pow(100,-0.5)*L;

    q1.SetVect(q1.E()*(e1*sin(Theta)*cos(Phi) + 
     e2*sin(Theta)*sin(Phi) + 
    e3*cos(Theta)) );
//cout <<"L\t" << L.X() <<"\t"<< L.Y() <<"\t"<< L.Z()<<"\n";
//cout <<"e3\t" << e3.X() <<"\t"<< e3.Y() <<"\t"<< e3.Z()<<"\n";
//cout<<"q1*q1 \t"<<q1*q1<<"\n";
//cout <<"q1.X \t"<<q1.X()<<"\t"<<"q1.Px \t"<<q1.Px()<<"\n";
    q2 = p1 + p2 - k1 - k2 - q1;
  //  cout << "q1^2 \t"<< pow(q1.E(),2)-   pow(q1.Px(),2)- pow(q1.Py(),2) - pow(q1.Pz(),2) << "\n";
  
   //cout <<"k2 \t"  << k2.Px() <<"\t"<< k2.Py()<<"\t"<<k2.Pz()<<"\t"<<k2.E()<<"\n";
    //cout << "q1 \t" << q1.Px() <<"\t"<< q1.Py()<<"\t"<<q1.Pz()<<"\t"<<q1.E()<<"\n";
   //cout <<  "q2 \t" << q2.Px() <<"\t"<<q2.Py()<<"\t"<<q2.Pz()<<"\t"<<q2.E()<<"\n";
    
    double err = q2*q2;
  // cout<< "|q2|^2 \t" << err << "\n";

    double sigma_D = Sigma (p1 , p2 , k1 , k2 , q1 , q2 , 'D');
    double sigma_M = Sigma (p1 , p2 , k1 , k2 , q1 , q2 , 'M');

    if (sigma_D > sigma_max_D)
      {
       sigma_max_D = sigma_D;
      } 

    if (sigma_M > sigma_max_M)
      {
       sigma_max_M = sigma_M;
      }
sigma_max = TMath::Max(sigma_max_D , sigma_max_M);

  //  cout << "sigma\t" << sigma << "\t";
    //  cout << "sigma_max\t" << sigma_max << "\n";
  TLorentzVector p1_;
  TLorentzVector p2_;
  TLorentzVector k1_;
  TLorentzVector k2_;
  TLorentzVector q1_;
  TLorentzVector q2_;
   
  p1_[3] = p1.E();
  p2_[3] = p2.E();
  k1_[3] = k1.E();
  k2_[3] = k2.E();
  q1_[3] = q1.E();
  q2_[3] = q2.E();

   p1_.SetVect(-1*p1.Vect());
   p2_.SetVect(-1*p2.Vect());
   k1_.SetVect(-1*k1.Vect());
   k2_.SetVect(-1*k2.Vect());
   q1_.SetVect(-1*q1.Vect());
   q2_.SetVect(-1*q2.Vect());
 // cout<<"k1.E\t"<<k1.E()<<"k1_\t"<<k1_.E()<<"\n";
//cout<<"k1.Px\t"<<k1.Px()<<"k1_.Px\t"<<k1_.Px()<<"\n";

  double sigma_ = Sigma (p1_ , p2_ , k1_ , k2_ , q1_ , q2_ , 'D');
//  cout <<"sigma \t"<<sigma<<"\t"<<"\t sigma_ \t"<<sigma_<<"\n";



 }
cout<<"So here we've found the max:))) \n";
cout<<"the second cycle: \n";
///////////////////////////////////////////
///////////////////////////////////////////
//  p1.SetVect(Par* p1.Vect());
//  p2.SetVect(Par* p2.Vect());


   TFile* hOutputFile1;
// TFile* hOutputFile2;
 //  TFile* hOutputFile3;
  // TFile* hOutputFile4;

  hOutputFile1 = new TFile("HistBothNu.root","RECREATE");
 // hOutputFile2 = new TFile("Angle_L2_initial_quark.root","RECREATE");
 // hOutputFile3 = new TFile("Min_angle_L1_final_quark.root","RECREATE");
 // hOutputFile4 = new TFile("Min_angle_L2_final_quark.root","RECREATE");

  TH1F *h1 = new TH1F("h1","Angle between L1 and initial quark I=0",19,0,TMath::Pi());
  TH1F *h2 = new TH1F("h2","Angle between L2 and initial quark",10,0,TMath::Pi());
  TH1F *h3 = new TH1F("h3","Minimal angle between L1 and final quark",10,0,TMath::Pi());
  TH1F *h4 = new TH1F("h4","Minimal angle between L2 and final quark",10,0,TMath::Pi());
 TH1F *h5 = new TH1F("h5","Angle between L1 and Z I=0",10,0,TMath::Pi() );
  TH1F *h6 = new TH1F("h6","Angle between L2 and Z",10,0,TMath::Pi() );
 TH1F *h7 = new TH1F("h7","k1_x",19,-E ,E);
  TH1F *h8 = new TH1F("h8","k1_y",19,-E, E);
  TH1F *h9 = new TH1F("h9","k1_z",19,-E , E);
 TH1F *h10 = new TH1F("h10","k2_x",19,-E ,E);
  TH1F *h11 = new TH1F("h11","k2_y",19,-E, E);
  TH1F *h12 = new TH1F("h12","k2_z",19,-E , E);
 TH1F *h13 = new TH1F("h13","Perpendicular momentum of L1",10,0,E );
  TH1F *h14 = new TH1F("h14","Perpendicular momentum of L2",10,0,E ); 
  TH1F *h15 = new TH1F("h15","Minimal angle between a lepton and a quark",10,0,TMath::Pi() );
 TH1F *h16 = new TH1F("h16","Angle between L1 and Z I=1",10,0,TMath::Pi() );
    TH1F *h17 = new TH1F("h17","Angle between L1 and initial quark I=1",19,0,TMath::Pi());
  TH1F *h18 = new TH1F("h18","Eta-phi distance between L1 and final quark ",10,0.,5.);
  TH1F *h19 = new TH1F("h19","Eta-phi distance between L2 and final quark ",10,0.,5.);
  TH1F *h20 = new TH1F("h20","Eta-phi distance between a lepton and final quark ",10,0.,5.);
  
  h1->Sumw2();
  h2->Sumw2();
  h3->Sumw2();
  h4->Sumw2();
  h5->Sumw2();
  h6->Sumw2();
  h7->Sumw2();
  h8->Sumw2();
  h9->Sumw2();
  h10->Sumw2();
  h11->Sumw2();
  h12->Sumw2();
  h13->Sumw2();
  h14->Sumw2();
  h15->Sumw2();
  h16->Sumw2();
  h17->Sumw2();
  h18->Sumw2();
  h19->Sumw2();
  h20->Sumw2();


  TVector3 Ax(0,0,1); 
int I =0;
for(int I = 0 ; I < 2 ; I++)
{
// p1(0,0,E,E);
  // p2(0,0,-E,E);
if (I == 1)
  {
//  p1 = TLorentzVector(-E,0,0,E)
  //p1[2] = -E;
  //p2[2]=E;
    Par=-1;
    p1.SetVect(Par* p1.Vect());
    p2.SetVect(Par* p2.Vect());

  }


 for(int j = 0 ; j < N ; j++)
  {
  //  cout<<"I \t"<<I << "\t"<<"j"<<j<<"\n";
  //  cout<< "k1_max_x\t"<< k1_max.Px()<<"\n";
    //cout<< "k1_max_x\t"<< k1_max.Px()<<"\n";
    // cout<< "k1_max_x\t"<< k1_max.Px()<<"\n";


    k1[0] =  Par*r.Uniform(-E , E);
//    k1_max[1] = sqrt(pow(k1_max.E(),2) - pow(k1.Px(),2)  );
    k1[1] =  Par*r.Uniform(-E , E);
//    k1_max[2] = sqrt(pow(k1_max.E(),2) - pow(k1.Px(),2) -pow(k1.Py(),2));
    k1[2] =  Par*r.Uniform(-E , E);
    k1[3] = sqrt(pow(k1.Px() , 2) + pow(k1.Py() , 2 ) + pow(k1.Pz() , 2));
//     cout<<"k1\t" << k1.Px() <<"\t"<< k1.Py()<<"\t"<<k1.Pz()<<"\t"<<k1.E()<<"\n";


    if (k1.E() > E ) continue;



//    k2_max[3] = p1.E()+p2.E() - k1.E();
//    if (k1.Px() >= 0 )   k2_max[0] = E -  k1.Px();
  //    else
 k2_max[0] = E;
//    if (k1.Px() <= 0)  k2_min[0] = -E -  k1.Px();
  //    else 
k2_min[0] = -E;


    k2(0) =  Par*r.Uniform(k2_min.Px() , k2_max.Px());
  //  k2_max[1] = TMath::Min( sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2)) -k1.Py()   ,sqrt(pow(E,2) - pow(k2.Px(),2)  )   );
  //    k2_min[1] = TMath::Max(  - sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2)) -k1.Py()  ,   -sqrt(pow(E,2) - pow(k2.Px(),2)  )       );
    k2_max[1] = E;  
    k2_min[1] = -E;
  k2(1) =  Par*r.Uniform(k2_min.Py() , k2_max.Py());
    //k2_max[2] = TMath::Min(sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2) -pow(k1.Py()+k2.Py(),2)) - k1.Py()   ,   sqrt(pow(E,2) -pow(k2.Px(),2) -pow(k2.Py(),2))   );
   // k2_min[1] = TMath::Max(- sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2) -pow(k1.Py()+k2.Py(),2)) - k1.Py()   ,  - sqrt(pow(E,2) -pow(k2.Px(),2) - pow(k2.Py(),2))  );
    k2_max[2] = E;
    k2_min[2] = -E;
k2(2) =  Par*r.Uniform(k2_min.Pz() , k2_max.Pz());
    k2(3) = sqrt(pow(k2.Px() , 2) + pow(k2.Py() , 2 ) + pow(k2.Pz() , 2));
    
 


if (k2.E() > p1.E() + p2.E() - k1.E() ) continue;
   if (k2.E() > E ) continue;


//  q1_max[3] =  TMath::Min(  p1.E()+p2.E() - k1.E()-k2.E()  ,  E);
    q1_max[3] = E;
//    q1_max[0] = q1_max.E();
//    q1_max[1] = q1_max.E();
//    q1_max[2] = q1_max.E();

      TVector3 k1_spa = k1.Vect();
      TVector3 k2_spa = k2.Vect();
      TVector3 L = k1_spa + k2_spa;
     // L(3) = sqrt(pow(L.Px() , 2) + pow(L.Py() , 2 ) + pow(L.Pz() , 2));

    double Theta =  r.Uniform(0 , TMath::Pi());
     double Phi = r.Uniform(0 , 2*TMath::Pi());


    q1(3) = (pow(2*E-sqrt(k1_spa*k1_spa)-sqrt(k2_spa*k2_spa) ,2) -
(k1_spa+k2_spa)*(k1_spa+k2_spa)  ) *pow(2*( 2*E - sqrt(k1_spa*k1_spa) -
sqrt(k2_spa*k2_spa) + sqrt((k1_spa+k2_spa)*(k1_spa+k2_spa))*cos(Theta)),-1);

//h3->Fill( k1.Py());

    if (q1.E() < 0)
      {
     //cout << "q1.E < 0 \n";
      continue;
      }
  

  if ( p1.E() + p2.E() - k1.E() - k2.E() - q1.E() < 0) continue;
    TVector3 e1;
    TVector3 e2;
    TVector3 e3;
    e1(0) = 0;
    e1(1) = L.Z()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
    e1(2) = -L.Y()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
//cout <<"e1\t"  << e1.X() <<"\t"<< e1.Y() <<"\t"<< e1.Z()<<"\n";

  e2(0) = (pow(L.Y(),2) + pow(L.Z(),2))*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);
    e2(1) = -L.X()*L.Y()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);
    e2(2) = -L.X()*L.Z()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);

//cout <<"e2\t" << e2.X() <<"\t"<< e2.Y() <<"\t"<< e2.Z()<<"\n";

//cout<<"phi\t"<<Phi<<"\n";
    e3 = pow( (L*L) ,-0.5)* L;
//e3 = pow(100,-0.5)*L;

    q1.SetVect(q1.E()*(e1*sin(Theta)*cos(Phi) + //+ before e2 is changed to- if all momenta change signs
     Par*e2*sin(Theta)*sin(Phi) +
    e3*cos(Theta)) );


    q2 = p1 + p2 - k1 - k2 - q1;
  //  cout << "q1^2 \t"<< pow(q1.E(),2)-   pow(q1.Px(),2)- pow(q1.Py(),2) - pow(q1.Pz(),2) << "\n";

   //cout <<"k2 \t"  << k2.Px() <<"\t"<< k2.Py()<<"\t"<<k2.Pz()<<"\t"<<k2.E()<<"\n";
    //cout << "q1 \t" << q1.Px() <<"\t"<< q1.Py()<<"\t"<<q1.Pz()<<"\t"<<q1.E()<<"\n";
   //cout <<  "q2 \t" << q2.Px() <<"\t"<<q2.Py()<<"\t"<<q2.Pz()<<"\t"<<q2.E()<<"\n";

    //double err = q2*q2;
  // cout<< "|q2|^2 \t" << err << "\n";

    double sigma_D = Sigma (p1 , p2 , k1 , k2 , q1 , q2 , 'D');
    double sigma_M = Sigma (p1 , p2 , k1 , k2 , q1 , q2 , 'M');

    if (sigma_D > sigma_max_D   )
      {
    //  sigma_max_D = sigma_D;
      cout << "Warnnig: we've got sigma > sigma_max found in the first cycle \n"  ;
      }
 
     double Prob = r.Uniform(0 , 1);


  //  cout << "sigma\t" << sigma << "\n";
   // cout << "sigma_max\t" << sigma_max << "\n";
   // cout << "Probability\t" << Prob << "\n";

    
    if (sigma_D >2* sigma_max*Prob ||  sigma_M >2* sigma_max*Prob )
      {  
      TVector3 p1_s = p1.Vect();
      TVector3 k1_s = k1.Vect();
       TVector3 k2_s = k2.Vect();
       double theta_L1 = p1_s.Angle(k1_s); 
       double theta_L2 = p1_s.Angle(k2_s);
       cout<<"I\t"<<I<<"\n";
       cout<<"j"<<j<<"\n";
       cout<<"sigma_D"<<sigma_D<<"\n";
       cout<<"sigma_max"<<sigma_max<<"\n";
       cout<<"Prob"<<Prob<<"\n";
       cout <<"Theta_L1\t"<< theta_L1 <<"\n";
       cout <<"Theta_L2\t"<< theta_L2 <<"\n";
       if (I == 0 ) h1->Fill( theta_L1);
        if (I == 1 ) h17->Fill( theta_L1);

       h2->Fill( theta_L2);
   

      TVector3 q1_s = q1.Vect();
      TVector3 q2_s = q2.Vect();
      double theta_L1_f = TMath::Min(  q1_s.Angle(k1_s)  ,  q2_s.Angle(k1_s)  );
      double theta_L2_f = TMath::Min(  q1_s.Angle(k2_s)  ,  q2_s.Angle(k2_s)  );
       cout <<"Theta_L1_f"<< theta_L1_f <<"\n";
       cout <<"Theta_L2_f"<< theta_L2_f <<"\n";
       cout<<"p1.z \t"<<p1.Pz()<<"\n";
        cout<<"p2.z \t"<<p2.Pz()<<"\n";
    //   cout<<"I"<<I <<"\n";
      h3->Fill( theta_L1_f);
      h4->Fill( theta_L2_f);

       double theta_L1_Z = Ax.Angle(k1_s);
       double theta_L2_Z = Ax.Angle(k2_s);
       if(I == 0)h5->Fill( theta_L1_Z);
       if(I == 1)h16->Fill( theta_L1_Z);


       h6->Fill( theta_L2_Z);
       
       h7->Fill( k1.Px());
       h8->Fill( k1.Py());
       h9->Fill( k1.Pz());
       h10->Fill( k2.Px());
       h11->Fill( k2.Py());
       h12->Fill( k2.Pz());
       h13->Fill( sqrt( pow(k1.Px() ,2)+ pow(k1.Py() ,2)  )  );
       h14->Fill( sqrt( pow(k2.Px() ,2)+ pow(k2.Py() ,2)  )  );
       h15->Fill( TMath::Min( theta_L1_f , theta_L2_f ));
   
       double dist11 = sqrt( pow(k1_s.PseudoRapidity() - q1_s.PseudoRapidity(),2) +  pow(k1_s.Phi() - q1_s.Phi(),2)    );  
        double dist12 = sqrt( pow(k1_s.PseudoRapidity() - q2_s.PseudoRapidity(),2) +  pow(k1_s.Phi() - q2_s.Phi(),2)    );
        double dist21 = sqrt( pow(k2_s.PseudoRapidity() - q1_s.PseudoRapidity(),2) +  pow(k2_s.Phi() - q1_s.Phi(),2)    );
        double dist22 = sqrt( pow(k2_s.PseudoRapidity() - q2_s.PseudoRapidity(),2) +  pow(k2_s.Phi() - q2_s.Phi(),2)    );
       
       if ( fabs(  k1_s.PseudoRapidity()  )>5   ) continue;
       if ( fabs(  k2_s.PseudoRapidity()  )>5   ) continue;
       if ( fabs(  q1_s.PseudoRapidity()  )>5   ) continue;
       if ( fabs(  q2_s.PseudoRapidity()  )>5   ) continue;

        double dist1 = TMath::Min(dist11 , dist12);
        double dist2 = TMath::Min(dist21 , dist22);
        double dist = TMath::Min(dist1 , dist2);
       h18->Fill(dist1  );
       h19->Fill(dist2 );
       h20->Fill(dist  );

       // cout<<"k1.E\t"<<k1.E()<<"\n";
       // cout<<"k1.Px\t"<<k1.Px()<<"\n";

      }
  
  }
}
  TCanvas *c1 = new TCanvas("c1","c1",800,1000);
  h1->Draw();
  h1->Write();
  TCanvas *c2 = new TCanvas("c2","c2",800,1000);
  h2->Draw();
  h2->Write();
   TCanvas *c3 = new TCanvas("c3","c3",800,1000);
  h3->Draw();
  h3->Write();
  TCanvas *c4 = new TCanvas("c4","c4",800,1000);
  h4->Draw();
  h4->Write();
   TCanvas *c5 = new TCanvas("c5","c5",800,1000);
  h5->Draw();
  h5->Write();
  TCanvas *c6 = new TCanvas("c6","c6",800,1000);
  h6->Draw();
  h6->Write();
  TCanvas *c7 = new TCanvas("c7","c7",800,1000);
  h7->Draw();
  h7->Write();
  TCanvas *c8 = new TCanvas("c8","c8",800,1000);
  h8->Draw();
  h8->Write();
   TCanvas *c9 = new TCanvas("c9","c9",800,1000);
  h9->Draw();
  h9->Write();
  TCanvas *c10 = new TCanvas("c10","c10",800,1000);
  h10->Draw();
  h10->Write();
  TCanvas *c11 = new TCanvas("c11","c11",800,1000);
  h11->Draw();
  h11->Write();
  TCanvas *c12 = new TCanvas("c12","c12",800,1000);
  h12->Draw();
  h12->Write();
   TCanvas *c13 = new TCanvas("c13","c13",800,1000);
  h13->Draw();
  h13->Write();
  TCanvas *c14 = new TCanvas("c14","c14",800,1000);
  h14->Draw();
  h14->Write();
  TCanvas *c15 = new TCanvas("c15","c15",800,1000);
  h15->Draw();
  h15->Write();
  TCanvas *c16 = new TCanvas("c16","c16",800,1000);
  h16->Draw();
  h16->Write();
  TCanvas *c17 = new TCanvas("c17","c17",800,1000);
  h17->Draw();
  h17->Write();
  TCanvas *c18 = new TCanvas("c18","c18",800,1000);
  h18->Draw();
  h18->Write();
  TCanvas *c19 = new TCanvas("c19","c19",800,1000);
  h19->Draw();
  h19->Write();
  TCanvas *c20 = new TCanvas("c20","c20",800,1000);
  h20->Draw();
  h20->Write();


//f->Write();
//f->Close();
  hOutputFile1->Write();
  hOutputFile1->Close();
//  hOutputFile2->Write();
  //hOutputFile2->Close();
//   hOutputFile3->Write();
  //hOutputFile3->Close();
//  hOutputFile4->Write();
  //hOutputFile4->Close();




  return 0 ;
}
