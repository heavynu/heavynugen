#include <iostream>
///#include "CLHEP/Vector/LorentzVector.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include <cmath>
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TInterpreter.h"
#include "TROOT.h"

//using namespace TMath;
using namespace std;

double M_W = 1000;
double M_nu = 500;
double Gamma_W=2;//The same as in SM
double Gamma_nu=1;//random
double M_L2 = 0.0005; //It should be non-zero otherwise Omega_2 diverges


double Omega_1 (TLorentzVector p_1  ,TLorentzVector  k_1)
{  
   TVector3 p_1_Spatial = p_1.Vect();
   TVector3 k_1_Spatial = k_1.Vect();
   double theta =  k_1_Spatial.Angle(p_1_Spatial);
   double DOmega = sin(theta)*pow( 64*pow(TMath::Pi(),2)*M_W ,-1);
   return DOmega;
}


double Omega_2 ( TLorentzVector k_2  ,TLorentzVector  q_1 , TLorentzVector  q_2)
{
     double alpha = pow(137,-1);
  double SinTheta_W = sqrt(0.23);
  double e = sqrt(4*TMath::Pi()*alpha);
  double g = e/SinTheta_W;


    TVector3 k_2_S = k_2.Vect();
   TVector3 q_1_S = q_1.Vect();
   TVector3 q_2_S = q_2.Vect();
   double theta =  q_1_S.Angle(q_1_S + q_2_S);
   
   TLorentzVector L = k_2 + q_1 + q_2; 
    double M_squared = 2*pow(g,4)*(L*q_1)*(k_2*q_2) *  
pow(pow((q_1+q_2)*(q_1+q_2) - M_W*M_W,2)+ pow(M_W *Gamma_W ,2)  ,-1);
    double DOmega =  M_squared *    sqrt(q_1_S*q_1_S) * sin(theta)*pow( 
16*pow(2*TMath::Pi(),5)* ( sqrt(q_1_S*q_1_S) + sqrt(q_2_S*q_2_S) + 
sqrt((q_1_S+q_2_S)*(q_1_S+q_2_S))*cos(theta) )* sqrt((k_2_S+q_1_S+q_2_S)*(k_2_S+q_1_S+q_2_S))*k_2.E()  ,-1);
   cout << theta << "\n";   
return DOmega;
}




double Sigma(TLorentzVector p_1 , TLorentzVector p_2 ,TLorentzVector  k_1,
TLorentzVector  k_2 ,  TLorentzVector  q_1 , TLorentzVector q_2 )
{
  double alpha = pow(137,-1);
  //alpha = 1/137;
  double SinTheta_W = sqrt(0.23);
  double e = sqrt(4*TMath::Pi()*alpha);
   //cout << "e\t"<< e <<"\n";
   //cout << "pi\t"<< TMath::Pi()<<"\n";
   //cout << "alpha\t"<< alpha <<"\n";


  double g = e/SinTheta_W;    
  }

int main ()
{
  double E = M_W*0.5;  //The energy of initial quark. It is such because W should be on-shell
  TLorentzVector p1(0,0,E,E); 
  TLorentzVector p2(0,0,-E,E);
  //cout << p1.Px() <<"\t"<< p1.Py()<<"\t"<<p1.Pz()<<"\t"<<p1.E()<<"\n";
   //cout << p2.Px() <<"\t"<< p2.Py()<<"\t"<<p2.Pz()<<"\t"<<p2.E()<<"\n";

  TLorentzVector k1;
  TLorentzVector k2;
  TLorentzVector q1;
  TLorentzVector q2;

  TLorentzVector k1_max(E,E,E,E);
  TLorentzVector k2_max; 
   TLorentzVector k2_min;
 //(E,E,E,E);
  TLorentzVector q1_max;
    //(E,E,E,E);
 // TLorentzVector q2_max;//(E,E,E,E);

  double Omega_1_max=0;
  double Omega_2_max=0;

   TRandom3 r;

  int N=40000000; //Number of steps
 //  int N = 100000;  
for(int i = 0 ; i < N ; i++)
  {
    //cout<<"The first cycle. i \t"<< i <<"\n";
    double Theta1 =  r.Uniform( 0 , TMath::Pi() );
    double Phi1 =  r.Uniform( 0 , 2*TMath::Pi() );
    k1[3] = (pow(M_W,2) - pow(M_nu,2)) * pow( 2*M_W ,-1);
    if (k1.E() < 0) 
      {
      cout <<"Error: k1.E <0 \n";
      break;
      } 

    k1[0] = k1.E()*sin(Theta1)*cos(Phi1);
    k1[1] = k1.E()*sin(Theta1)*sin(Phi1);
    k1[2] = k1.E()*cos(Theta1);

//     cout<<"k1\t" << k1.Px() <<"\t"<< k1.Py()<<"\t"<<k1.Pz()<<"\t"<<k1.E()<<"\n";

//    k2_max[3] = p1.E()+p2.E() - k1.E();
    if (k1.Px() >= 0 )   k2_max[0] = E -  k1.Px();
      else k2_max[0] = E;
    if (k1.Px() <= 0)  k2_min[0] = -E -  k1.Px();
      else k2_min[0] = -E;


    k2(0) = r.Uniform(k2_min.Px() , k2_max.Px());
    k2_max[1] = TMath::Min( sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2)) - 
k1.Py()   , 
sqrt(pow(E,2) - pow(k2.Px(),2)  )   );
      k2_min[1] = TMath::Max(  - sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2)) - 
k1.Py()  ,   -sqrt(pow(E,2) - pow(k2.Px(),2)  )       );
    k2(1) = r.Uniform(k2_min.Py() , k2_max.Py());
    k2_max[2] = TMath::Min(sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2) - 
pow(k1.Py()+k2.Py(),2)) - k1.Py()   ,   sqrt(pow(E,2) - 
pow(k2.Px(),2) -pow(k2.Py(),2))   );
    k2_min[1] = TMath::Max(- sqrt(pow(E,2) - pow(k1.Px()+k2.Px(),2) - 
pow(k1.Py()+k2.Py(),2)) - k1.Py()   ,  - sqrt(pow(E,2) - 
pow(k2.Px(),2) - pow(k2.Py(),2))  );
    k2(2) = r.Uniform(k2_min.Pz() , k2_max.Pz());
    k2(3) = sqrt(pow(k2.Px() , 2) + pow(k2.Py() , 2 ) + pow(k2.Pz() , 2) + pow(M_L2,2));
    if (k2.E() > p1.E() + p2.E() - k1.E() ) continue;

  //     cout<<"k2\t" << k2.Px() <<"\t"<<k2.Py()<<"\t"<<k2.Pz()<<"\t"<<k2.E()<<"\n";
  // cout<<"k2_max\t" << k2_max.Px() <<"\t"<<k2_max.Py()<<"\t"<<k2_max.Pz()<<"\t"<<k2_max.E()<<"\n";
//cout << "k2^2 \t"<<k2*k2 <<"\n";


   q1_max[3] =  TMath::Min(  p1.E()+p2.E() - k1.E()-k2.E()  ,  E);
//    q1_max[0] = q1_max.E();
//    q1_max[1] = q1_max.E();
//    q1_max[2] = q1_max.E();

      TVector3 k1_spa = k1.Vect();
      TVector3 k2_spa = k2.Vect();
      TVector3 L = k1_spa + k2_spa;
     // L(3) = sqrt(pow(L.Px() , 2) + pow(L.Py() , 2 ) + pow(L.Pz() , 2));
  
    double Theta =  r.Uniform(0 , TMath::Pi());
     double Phi = r.Uniform(0 , 2*TMath::Pi());
    

    q1(3) = (pow(2*E-sqrt(k1_spa*k1_spa)-sqrt(k2_spa*k2_spa) ,2) - 
(k1_spa+k2_spa)*(k1_spa+k2_spa)  ) *pow(2*( 2*E - sqrt(k1_spa*k1_spa) - 
sqrt(k2_spa*k2_spa) + sqrt((k1_spa+k2_spa)*(k1_spa+k2_spa))*cos(Theta)  
),-1);
    if (q1.E() < 0) 
      {
     //cout << "q1.E < 0 \n";
      continue;
      }
    if ( p1.E() + p2.E() - k1.E() - k2.E() - q1.E() < 0) continue;
    TVector3 e1;
    TVector3 e2;
    TVector3 e3;
    e1(0) = 0;
    e1(1) = L.Z()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
    e1(2) = -L.Y()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
//cout <<"e1\t"  << e1.X() <<"\t"<< e1.Y() <<"\t"<< e1.Z()<<"\n";

    e2(0) = (pow(L.Y(),2) + pow(L.Z(),2))*pow( pow(pow(L.Y(),2) + 
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5); 
    e2(1) = -L.X()*L.Y()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);
    e2(2) = -L.X()*L.Z()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);

//cout <<"e2\t" << e2.X() <<"\t"<< e2.Y() <<"\t"<< e2.Z()<<"\n";
  
//cout<<"phi\t"<<Phi<<"\n";
    e3 = pow( (L*L) ,-0.5)* L;
//e3 = pow(100,-0.5)*L;

    q1.SetVect(q1.E()*(e1*sin(Theta)*cos(Phi) + 
    e2*sin(Theta)*sin(Phi) + 
    e3*cos(Theta)) );
//cout <<"L\t" << L.X() <<"\t"<< L.Y() <<"\t"<< L.Z()<<"\n";
//cout <<"e3\t" << e3.X() <<"\t"<< e3.Y() <<"\t"<< e3.Z()<<"\n";
//cout<<"q1*q1 \t"<<q1*q1<<"\n";
//cout <<"q1.X \t"<<q1.X()<<"\t"<<"q1.Px \t"<<q1.Px()<<"\n";
    q2 = p1 + p2 - k1 - k2 - q1;
  //  cout << "q1^2 \t"<< pow(q1.E(),2)-   pow(q1.Px(),2)- pow(q1.Py(),2) - pow(q1.Pz(),2) << "\n";
  
   //cout <<"k2 \t"  << k2.Px() <<"\t"<< k2.Py()<<"\t"<<k2.Pz()<<"\t"<<k2.E()<<"\n";
    //cout << "q1 \t" << q1.Px() <<"\t"<< q1.Py()<<"\t"<<q1.Pz()<<"\t"<<q1.E()<<"\n";
   //cout <<  "q2 \t" << q2.Px() <<"\t"<<q2.Py()<<"\t"<<q2.Pz()<<"\t"<<q2.E()<<"\n";
    
    double err = q2*q2;
  // cout<< "|q2|^2 \t" << err << "\n";

    double Omega1 = Omega_1(p1,k1);
    double Omega2 = Omega_2(k2 , q1 , q2);
    if (Omega1  > Omega_1_max)
      {
       Omega_1_max = Omega1;
       cout << "Omega_1_max\t" << Omega_1_max << "\n";
      } 

     if (Omega2 > Omega_2_max)
      {
       Omega_2_max = Omega2;
       cout << "Omega_2_max\t" << Omega_2_max << "\n";
      }

  //  cout << "sigma\t" << sigma << "\n";
//    cout << "Omega2\t" << Omega_2_max << "\n";
  }
cout<<"So here we've found the max \n";
cout<<"the second cycle: \n";
///////////////////////////////////////////
///////////////////////////////////////////
   TFile* hOutputFile;
  hOutputFile = new TFile("Hist_decay.root","RECREATE");
  TH1F *h1 = new TH1F("h1","Angle between L1 and initial quark",10,0,TMath::Pi());
  TH1F *h2 = new TH1F("h2","Angle between L2 and initial quark",10,0,TMath::Pi());
  TH1F *h3 = new TH1F("h3","Minimal angle between L1 and final quark",10,0,TMath::Pi());
  TH1F *h4 = new TH1F("h4","Minimal angle between L2 and final quark",10,0,TMath::Pi());
   TH1F *h5 = new TH1F("h5","Angle between L1 and Z",10,0,TMath::Pi() );
  TH1F *h6 = new TH1F("h6","Angle between L2 and Z",10,0,TMath::Pi() );
 TH1F *h7 = new TH1F("h7","k1_x",19,-E ,E);
  TH1F *h8 = new TH1F("h8","k1_y",19,-E, E);
  TH1F *h9 = new TH1F("h9","k1_z",19,-E , E);
 TH1F *h10 = new TH1F("h10","k2_x",19,-E ,E);
  TH1F *h11 = new TH1F("h11","k2_y",19,-E, E);
  TH1F *h12 = new TH1F("h12","k2_z",19,-E , E);
 TH1F *h13 = new TH1F("h13","Perpendicular momentum of L1",10,0,E );
  TH1F *h14 = new TH1F("h14","Perpendicular momentum of L2",10,0 , E );
  TH1F *h15 = new TH1F("h15","Minimal angle between a lepton and a quark",10,0,TMath::Pi()  );
  TH1F *h18 = new TH1F("h18","Eta-phi distance between L1 and final quark ",10,0.,5.);
  TH1F *h19 = new TH1F("h19","Eta-phi distance between L2 and final quark ",10,0.,5.);
  TH1F *h20 = new TH1F("h20","Eta-phi distance between a lepton and final quark ",10,0.,5.);

  h1->Sumw2(); 
  h2->Sumw2();
  h3->Sumw2();
  h4->Sumw2();
  h5->Sumw2();
  h6->Sumw2();
  h7->Sumw2();
  h8->Sumw2();
  h9->Sumw2();
  h10->Sumw2();
  h11->Sumw2();
  h12->Sumw2();
  h13->Sumw2();
  h14->Sumw2();
  h15->Sumw2();
  h18->Sumw2();
  h19->Sumw2();
  h20->Sumw2();

//////////////////////////////////////////////////
//////////////////////////////////////////////
///////////////////////////////////////
  TVector3 Ax(0,0,1);


  for(int j = 0 ; j < N ; j++)
  {
    double Theta1 =  r.Uniform( 0 , TMath::Pi() );
    double Phi1 =  r.Uniform( 0 , 2*TMath::Pi() );
    k1[3] = (pow(M_W,2) - pow(M_nu,2)) * pow( 2*M_W ,-1);
    if (k1.E() < 0)
      {
      cout <<"Error: k1.E <0 \n";
      break;
      }

    k1[0] = k1.E()*sin(Theta1)*cos(Phi1);
    k1[1] = k1.E()*sin(Theta1)*sin(Phi1);
    k1[2] = k1.E()*cos(Theta1);

//     cout<<"k1\t" << k1.Px() <<"\t"<< k1.Py()<<"\t"<<k1.Pz()<<"\t"<<k1.E()<<"\n";

//    if (k1.E() > p1.E() + p2.E() ) continue;

//    k2_max[3] = p1.E()+p2.E() - k1.E();
//    if (k1.Px() >= 0 )   k2_max[0] = E -  k1.Px();
  //    else 
    k2_max[0] = E;
    //if (k1.Px() <= 0)  k2_min[0] = -E -  k1.Px();
     // else 
    k2_min[0] = -E;


    k2(0) = r.Uniform(k2_min.Px() , k2_max.Px());
    k2_max[1] = E;
     k2_min[1] = -E;

    k2(1) = r.Uniform(k2_min.Py() , k2_max.Py());
    k2_max[2] = E;
    k2_min[2] = -E;
    k2(2) = r.Uniform(k2_min.Pz() , k2_max.Pz());
    k2(3) = sqrt(pow(k2.Px() , 2) + pow(k2.Py() , 2 ) + pow(k2.Pz() , 2));
    if (k2.E() > p1.E() + p2.E() - k1.E() ) continue;
       if (k2.E() > E ) continue;

  q1_max[3] = E;
//    q1_max[0] = q1_max.E();
//    q1_max[1] = q1_max.E();
//    q1_max[2] = q1_max.E();

      TVector3 k1_spa = k1.Vect();
      TVector3 k2_spa = k2.Vect();
      TVector3 L = k1_spa + k2_spa;
     // L(3) = sqrt(pow(L.Px() , 2) + pow(L.Py() , 2 ) + pow(L.Pz() , 2));

    double Theta =  r.Uniform(0 , TMath::Pi());
     double Phi = r.Uniform(0 , 2*TMath::Pi());


    q1(3) = (pow(2*E-sqrt(k1_spa*k1_spa)-sqrt(k2_spa*k2_spa) ,2) -
(k1_spa+k2_spa)*(k1_spa+k2_spa)  ) *pow(2*( 2*E - sqrt(k1_spa*k1_spa) -
sqrt(k2_spa*k2_spa) + sqrt((k1_spa+k2_spa)*(k1_spa+k2_spa))*cos(Theta)
),-1);
    if (q1.E() < 0)
      {
     //cout << "q1.E < 0 \n";
      continue;
      }
    if ( p1.E() + p2.E() - k1.E() - k2.E() - q1.E() < 0) continue;
    TVector3 e1;
    TVector3 e2;
    TVector3 e3;
    e1(0) = 0;
    e1(1) = L.Z()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
    e1(2) = -L.Y()*pow(pow(L.Y(),2) + pow(L.Z(),2),-0.5);
//cout <<"e1\t"  << e1.X() <<"\t"<< e1.Y() <<"\t"<< e1.Z()<<"\n";

  e2(0) = (pow(L.Y(),2) + pow(L.Z(),2))*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);
    e2(1) = -L.X()*L.Y()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);
    e2(2) = -L.X()*L.Z()*pow( pow(pow(L.Y(),2) +
    pow(L.Z(),2),2)  +  pow(L.X()*L.Y(),2)   +  pow(L.X()*L.Z(),2),-0.5);

//cout <<"e2\t" << e2.X() <<"\t"<< e2.Y() <<"\t"<< e2.Z()<<"\n";

//cout<<"phi\t"<<Phi<<"\n";
    e3 = pow( (L*L) ,-0.5)* L;
//e3 = pow(100,-0.5)*L;

    q1.SetVect(q1.E()*(e1*sin(Theta)*cos(Phi) +
    e2*sin(Theta)*sin(Phi) +
    e3*cos(Theta)) );


    q2 = p1 + p2 - k1 - k2 - q1;
  //  cout << "q1^2 \t"<< pow(q1.E(),2)-   pow(q1.Px(),2)- pow(q1.Py(),2) - pow(q1.Pz(),2) << "\n";

   //cout <<"k2 \t"  << k2.Px() <<"\t"<< k2.Py()<<"\t"<<k2.Pz()<<"\t"<<k2.E()<<"\n";
    //cout << "q1 \t" << q1.Px() <<"\t"<< q1.Py()<<"\t"<<q1.Pz()<<"\t"<<q1.E()<<"\n";
   //cout <<  "q2 \t" << q2.Px() <<"\t"<<q2.Py()<<"\t"<<q2.Pz()<<"\t"<<q2.E()<<"\n";

    //double err = q2*q2;
  // cout<< "|q2|^2 \t" << err << "\n";

//    double sigma = Sigma (p1 , p2 , k1 , k2 , q1 , q2);
  //  if (sigma > sigma_max)
    //  {
      //sigma_max = sigma;
     // cout << "Warnnig: we've got sigma > sigma_max found in the first cycle \n"  ;
      //}
 
     double Prob = r.Uniform(0 , 1);
     // Omega_2_max = Omega2;
      // The first decay takes place by all means so we don't consider its probability

    //cout << "sigma\t" << sigma << "\n";
    //cout << "sigma_max\t" << sigma_max << "\n";
    //cout << "Probability\t" << Prob << "\n";

       double Omega2 = Omega_2(k2 , q1 , q2);    
    if (Omega2 > Omega_2_max*Prob  )
      {  
      TVector3 p1_s = p1.Vect();
      TVector3 k1_s = k1.Vect();
       TVector3 k2_s = k2.Vect();
       TVector3 q1_s = q1.Vect();
       TVector3 q2_s = q2.Vect();

       double theta_L1 = p1_s.Angle(k1_s); 
       double theta_L2 = p1_s.Angle(k2_s);
       double theta_L1_f = TMath::Min( q1_s.Angle(k1_s) ,  
q2_s.Angle(k1_s) );
       double theta_L2_f = TMath::Min( q1_s.Angle(k2_s) ,  
q2_s.Angle(k2_s) );

       cout <<"Theta_L1"<< theta_L1 <<"\n";
       cout <<"Theta_L2"<< theta_L2 <<"\n";
       h1->Fill( theta_L1);
       h2->Fill( theta_L2);
       h3->Fill( theta_L1_f);
       h4->Fill( theta_L2_f);
       
       double theta_L1_Z = Ax.Angle(k1_s);
       double theta_L2_Z = Ax.Angle(k2_s);
       
       h5->Fill( theta_L1_Z);
       h6->Fill( theta_L2_Z);

       h7->Fill( k1.Px());
       h8->Fill( k1.Py());
       h9->Fill( k1.Pz());
       h10->Fill( k2.Px());
       h11->Fill( k2.Py());
       h12->Fill( k2.Pz());
       h13->Fill( sqrt( pow(k1.Px() ,2)+ pow(k1.Py() ,2)  )  );
       h14->Fill( sqrt( pow(k2.Px() ,2)+ pow(k2.Py() ,2)  )  );
       h15->Fill( TMath::Min( theta_L1_f , theta_L2_f ));
       h6->Fill( theta_L2_Z);


   double dist11 = sqrt( pow(k1_s.PseudoRapidity() - 
q1_s.PseudoRapidity(),2) +  pow(k1_s.Phi() - q1_s.Phi(),2)    );
        double dist12 = sqrt( pow(k1_s.PseudoRapidity() - 
q2_s.PseudoRapidity(),2) +  pow(k1_s.Phi() - q2_s.Phi(),2)    );
        double dist21 = sqrt( pow(k2_s.PseudoRapidity() - 
q1_s.PseudoRapidity(),2) +  pow(k2_s.Phi() - q1_s.Phi(),2)    );
        double dist22 = sqrt( pow(k2_s.PseudoRapidity() - 
q2_s.PseudoRapidity(),2) +  pow(k2_s.Phi() - q2_s.Phi(),2)    );

       if ( fabs(  k1_s.PseudoRapidity()  )>5   ) continue;
       if ( fabs(  k2_s.PseudoRapidity()  )>5   ) continue;
       if ( fabs(  q1_s.PseudoRapidity()  )>5   ) continue;
       if ( fabs(  q2_s.PseudoRapidity()  )>5   ) continue;

        double dist1 = TMath::Min(dist11 , dist12);
        double dist2 = TMath::Min(dist21 , dist22);
        double dist = TMath::Min(dist1 , dist2);
       h18->Fill(dist1  );
       h19->Fill(dist2 );
       h20->Fill(dist  );



       


      }
  }

  TCanvas *c1 = new TCanvas("c1","c1",800,1000);
  h1->Draw();
  h1->Write();
  TCanvas *c2 = new TCanvas("c2","c2",800,1000);
  h2->Draw();
  h2->Write();
   TCanvas *c3 = new TCanvas("c3","c3",800,1000);
  h3->Draw();
  h3->Write();
  TCanvas *c4 = new TCanvas("c4","c4",800,1000);
  h4->Draw();
  h4->Write();
  TCanvas *c5 = new TCanvas("c5","c5",800,1000);
  h5->Draw();
  h5->Write();
  TCanvas *c6 = new TCanvas("c6","c6",800,1000);
  h6->Draw();
  h6->Write();
  TCanvas *c7 = new TCanvas("c7","c7",800,1000);
  h7->Draw();
  h7->Write();
  TCanvas *c8 = new TCanvas("c8","c8",800,1000);
  h8->Draw();
  h8->Write();
   TCanvas *c9 = new TCanvas("c9","c9",800,1000);
  h9->Draw();
  h9->Write();
  TCanvas *c10 = new TCanvas("c10","c10",800,1000);
  h10->Draw();
  h10->Write();
  TCanvas *c11 = new TCanvas("c11","c11",800,1000);
  h11->Draw();
  h11->Write();
  TCanvas *c12 = new TCanvas("c12","c12",800,1000);
  h12->Draw();
  h12->Write();
   TCanvas *c13 = new TCanvas("c13","c13",800,1000);
  h13->Draw();
  h13->Write();
  TCanvas *c14 = new TCanvas("c14","c14",800,1000);
  h14->Draw();
  h14->Write();
  TCanvas *c15 = new TCanvas("c15","c15",800,1000);
  h15->Draw();
  h15->Write();
  TCanvas *c18 = new TCanvas("c18","c18",800,1000);
  h18->Draw();
  h18->Write();
  TCanvas *c19 = new TCanvas("c19","c19",800,1000);
  h19->Draw();
  h19->Write();
  TCanvas *c20 = new TCanvas("c20","c20",800,1000);
  h20->Draw();
  h20->Write();

//f->Write();
//f->Close();
  hOutputFile->Write();
  hOutputFile->Close();
 // hOutputFile2->Write();
 // hOutputFile2->Close();




  return 0 ;
}
