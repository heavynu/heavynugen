ROOT_PATH = /afs/cern.ch/sw/lcg/app/releases/ROOT/5.30.00/x86_64-slc5-gcc43-opt/root
MYCLHEPPATH = /afs/cern.ch/sw/lcg/external/clhep/2.1.0.1/x86_64-slc5-gcc43-opt

	

HeavyNuGen : HeavyNuGen.cc
	g++ $@.cc -o $@.exe \
	-I$(ROOT_PATH)/include  \
	-L$(ROOT_PATH) `root-config --libs` \
	-lgfortran -lgfortranbegin

	@echo export LHAPATH=$(LHAPDF_PATH)/../share/PDFsets > ldlp.sh
	@echo export ROOTSYS=$(ROOTSYS) >> ldlp.sh
